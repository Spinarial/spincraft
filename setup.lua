-- Lua 5.1+ base64 v3.0 (c) 2009 by Alex Kloss <alexthkloss@web.de>
-- licensed under the terms of the LGPL2

-- character table string
local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

-- encoding
function b64enc(data)
    return ((data:gsub('.', function(x) 
        local r,b='',x:byte()
        for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
        return r;
    end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (#x < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return b:sub(c+1,c+1)
    end)..({ '', '==', '=' })[#data%3+1])
end

-- decoding
function b64dec(data)
    data = string.gsub(data, '[^'..b..'=]', '')
    return (data:gsub('.', function(x)
        if (x == '=') then return '' end
        local r,f='',(b:find(x)-1)
        for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
        return r;
    end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
        if (#x ~= 8) then return '' end
        local c=0
        for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
        return string.char(c)
    end))
end

-- command line if not called as library
if (arg ~= nil) then
	local func = 'b64enc'
	for n,v in ipairs(arg) do
		if (n > 0) then
			if (v == "-h") then print "base64.lua [-e] [-d] text/data" break
			elseif (v == "-e") then func = 'b64enc'
			elseif (v == "-d") then func = 'b64dec'
			else print(_G[func](v)) end
		end
	end
else
	module('base64',package.seeall)
end


local char_to_hex = function(c)
  return string.format("%%%02X", string.byte(c))
end

local function urlencode(url)
  if url == nil then
    return
  end
  url = url:gsub("\n", "\r\n")
  url = url:gsub("([^%w ])", char_to_hex)
  url = url:gsub(" ", "+")
  return url
end

local hex_to_char = function(x)
  return string.char(tonumber(x, 16))
end

local urldecode = function(url)
  if url == nil then
    return
  end
  url = url:gsub("+", " ")
  url = url:gsub("%%(%x%x)", hex_to_char)
  return url
end

-- ref: https://gist.github.com/ignisdesign/4323051
-- ref: http://stackoverflow.com/questions/20282054/how-to-urldecode-a-request-uri-string-in-lua
-- to encode table as parameters, see https://github.com/stuartpb/tvtropes-lua/blob/master/urlencode.lua



nb_per_page = 20

url = "https://gitlab.com/api/v4/projects/18564042/repository/tree?recursive=true&ref=master"
url = url.."&per_page="..nb_per_page
url = url.."&_cacheid="..os.date('%Y%m%d%H:%M:%S')

screen = peripheral.wrap('right')
screen.clear()
screen.setTextScale(0.5)
screen.setCursorPos(1,1) 

term.redirect(screen)

function createFile(file)

    filePath = '/'..file['path']

    if fs.exists(filePath) == true then
        fs.delete(filePath)
    end

    if file['type'] == 'tree' then

        if fs.isDir(filePath) ~= true then
            fs.makeDir(filePath)
            print('Created folder '..filePath)
        end
    else

        fileRequest = http.request(
            "https://gitlab.com/api/v4/projects/18564042/repository/files/"..urlencode(file['path']).."?ref=master", 
            nil,
            {['PRIVATE-TOKEN'] = 'glpat-UyRUiP1p47pRhCwmdJYt'}
            )

        local fileEvent, fileUrl, fileHandle
        repeat
            fileEvent, fileUrl, fileHandle = os.pullEvent("http_success")
        until fileUrl == fileUrl

        newFileData = textutils.unserializeJSON(fileHandle.readAll())
        
        newFile = fs.open(filePath, 'w')
        newFile.write(b64dec(newFileData['content']))
        newFile.close()
        print('Created file '..filePath)
    end

end

requestNb = 1

repeat

    newRequest = http.request(url.."&page="..requestNb,nil,{
        ['PRIVATE-TOKEN'] = 'glpat-UyRUiP1p47pRhCwmdJYt',
        ['Cache-Control'] = 'max-age=5'
    })

    local event, url, handle
    repeat
        event, url, handle = os.pullEvent("http_success")
    until url == url

    print("Request n°"..requestNb)
    requestNb = requestNb + 1
    
    files = textutils.unserializeJSON(handle.readAll())
    handle.close()

    for i1,file in pairs(files) do
        createFile(file)
    end

until #files < nb_per_page

term.redirect(term.native())
