-- database.lua
-- Organise the tables in form of csv files and provide ways of accessing them

function database_create_table(tablename)

    database_check_setup()

    if(fs.exists(settings.get("DATABASE_TABLE_PATH").."/"..tablename)) then
        error("[DBB] Table \""..tablename.."\" already exist.")
        return nil
    end

    local file_pointer = fs.open(settings.get("DATABASE_TABLE_PATH")..tablename, "w")

    local file_return = false

    if(file_pointer) then
        file_return = true
    end

    file_pointer.close()

    return file_return
end


function database_find(tablename, value)
    
end

function database_check_setup()

    -- Check the tables folder, create it if it doesn't exist
    if(not (fs.exists(settings.get("DATABASE_TABLE_PATH") and fs.isDir(settings.get("DATABASE_TABLE_PATH")))) then
        fs.makeDir(settings.get("DATABASE_TABLE_PATH"))
    end

    -- Check the table _settings, create it if it doesn't exist
    if(not(fs.exists(settings.get("DATABASE_TABLE_PATH")../"_settings") and fs.isDir(settings.get("DATABASE_TABLE_PATH")../"_settings") == false)) then
        fs.open(settings.get("DATABASE_TABLE_PATH")../"_settings").close()
    end
end
