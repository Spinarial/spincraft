-- settings.lua
-- initiate and access the settings file
 
function settings_initiate()
 
    local is_initiated = true
   
    if(not fs.exists("settings.cfg")) then
       
       -- Set basic settings later
        is_initiated = false
       
        -- Create the file
        fs.open("settings.cfg", "w").close()
    end
   
    settings.load("settings.cfg")
   
   -- Wrapper of the get function, easier to understand
    settings.isSet = function(value)
       
        if(settings.get(value, nil) == nil) then
            return false
        else
            return true
        end
    end
       
    -- Basic settings
    if(not is_initiated) then
        settings.set("ROOT_PATH", "/spincraft/")
        settings.set("SETTINGS_PATH", settings.get("ROOT_PATH").."settings/")    
        settings.set("DATABASE_ROOT_PATH", settings.get("ROOT_PATH").."database/")    
        settings.set("DATABASE_TABLE_PATH", settings.get("DATABASE_ROOT_PATH").."tables/")
    end
   
end

settings_initiate()
