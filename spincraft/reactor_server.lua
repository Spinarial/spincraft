--require('buttons')
require('GUI_GRID')
require('helper')

rednet.open("right")

local defaultTextColor = colors.white
local defaultTextBackground = colors.black

local screen = peripheral.wrap("top")
local screenSizeWidth, screenSizeHeight = screen.getSize()
screen.setBackgroundColor(defaultTextBackground)
screen.setTextColor(defaultTextColor)
screen.setTextScale(0.5)
screen.clear()
screen.setCursorBlink(false)

local term_container = GUI_GRID(screen, 1,1, screenSizeWidth, screenSizeHeight, 
{
 {{1, 10, 1}, 5},
 {12},
 {{1, 10, 1}, 5}   
}
)

-- setup date window
local date_window = term_container:get(3, 3)

parallel.waitForAll(
    function()
        -- SETUP WINDOW API

        local title_window = term_container:get(1,2)
        local title_window_width, title_window_height = title_window.getSize()
        writeAt(title_window, 1,1, "Spin's nuclear monitoring center")

        parallel.waitForAll(
            function()
                -- CLOCK API
                local clock_api_window = term_container:get(1, 3)

                while true do
                    clock_api_window.clear()
                    clock_api_window.setCursorPos(1,1)
                    clock_api_window.write(textutils.formatTime( os.time(), true ))
                    sleep(1)
                end
            end
        )
    end,
    function()
        -- SET MAIN WINDOW CONTENT

        content_window = term_container:get(2,1)

        term_container:drawBorders()
        
        local screenSizeWidth, screenSizeHeight = content_window.getSize()

        local total_and_main_grid = GUI_GRID(content_window, 1,1, screenSizeWidth, screenSizeHeight, 
            {
                {1, 10, 1},
                {12},
            }
        )
        
        total_and_main_grid:drawBorders()
        
        local total_window = total_and_main_grid:get(1,2)
        local main_grid_window = total_and_main_grid:get(2,1)

        parallel.waitForAll(
            function()
                -- Total process

                
                local screenSizeWidth, screenSizeHeight = total_window.getSize()

                local total_status_window = GUI_GRID(total_window, 1,1, screenSizeWidth, screenSizeHeight, 
                    {
                        {1},
                        {1}  
                    }
                )

                local total_value_energy_window = total_status_window:get(1,1)
                local status_window = total_status_window:get(2,1)

                while true do
                        -- Get the data from all the reactors and do the math and display it in the window 
                end
            end,
            function()
                -- reactors and scan 

                local reactor_list = {}

                local function drawBorders()

                    local reactor_grid_array = {}
                    
                    for k,v in pairs(reactor_list) do
                        table.insert(reactor_grid_array, 1)
                    end

                    local reactor_status_window = GUI_GRID(status_window, 1,1, screenSizeWidth, screenSizeHeight, 
                        {
                            reactor_grid_array
                        }
                    )
                    reactor_status_window:drawBorders()

                    local reactor_length = table.length(reactor_list)

                    local button_add_reactor_window = reactor_status_window:get(1, reactor_length)


                    local button_add_reactor_window_width,  button_add_reactor_window_height = button_add_reactor_window.getSize()
                    
                    local scan_for_reactor_button = Button(
                        button_add_reactor_window,
                        math.floor(screenWidth/2),
                        buttonRowHeight,
                        10, 4, colors.white, "Scan"
                    )
                    scan_for_reactor_button:setup()

                end
            end
        )
    end
)

--[[
local reactorsList = {}
local reactorsListLength = 0

function scanForReactorsWindow(screen)

    local screenWidth, screenHeight = screen.getSize()

    local window = window.create(screen, 1,1, screenWidth, screenHeight, true)

    local initText = "reactors connected"

    local initTextWidthPos = math.floor(screenWidth/2) - math.floor(string.len(initText)/2) + 2

    local buttonRowHeight =  math.floor(screenHeight/2) + 5

    writeAt(window, initTextWidthPos, screenHeight/2, initText)
    writeAt(window, initTextWidthPos - 5, screenHeight/2, "0")

    local scanForReactorsButton = Button(
        window,
        math.floor(screenWidth/2) - 22,
        buttonRowHeight,
        20, 4, colors.white, "Scan for reactors"
    )

    local stopSearchingButton = Button(
        window,
        math.floor(screenWidth/2) + 2,
        buttonRowHeight,
        20,4, colors.white, "Next"
    )

    scanForReactorsButton:setup()
    stopSearchingButton:setup()

    while true do
        local event, side, x, y = os.pullEvent("monitor_touch")
        scanForReactorsButton:handleEvent(x, y, 
            function() 

                log("scanForReactorsButton triggered")

                parallel.waitForAny(
                    function()
                        for i=1, 3, 1 do
                            writeAt(window, initTextWidthPos - 5, screenHeight/2, ".")
                            sleep(1)
                            writeAt(window, initTextWidthPos - 5, screenHeight/2, "..")
                            sleep(1)
                            writeAt(window, initTextWidthPos - 5, screenHeight/2, "...")
                            sleep(1)
                            writeAt(window, initTextWidthPos - 5, screenHeight/2, "   ")
                        end
                    end,
                    function()
                        while true do
                            local id, message, protocol = rednet.receive("nuclear_reactor_monitoring")
                            
                            log("rednet "..message.." message received. ID: "..id)

                            if(message == "nuclear_reactor_monitoring_init") then
                                log("rednet message is init")

                                if(not table.hasValue(reactorsList, id)) then
                                    table.insert(reactorsList, id)
                                    reactorsListLength = reactorsListLength + 1
                                    rednet.send(id, "nuclear_reactor_monitoring_init_connected", "nuclear_reactor_monitoring")
                                    print("send nuclear_reactor_monitoring_init_connected rednet to "..id)
                                else
                                    log("ID: "..id.." is already connected. Ignoring.")
                                end
                            end
                        end
                    end
                )

                writeAt(window, initTextWidthPos - 5, screenHeight/2, tostring(reactorsListLength))
            end
        )
        stopSearchingButton:handleEvent(x, y, function() end)
        
    end

    return window
end

function main()
    local searchForReactorsWindows = scanForReactorsWindow(screen)
end


main()

--]]
