require('/dev/spincraft/autoload')

screen = peripheral.wrap('right')
screen.clear()
screen.setTextScale(0.5)
screen.setCursorPos(1,1)

term.redirect(screen)

print_r(peripheral.getMethods('back'))

print_r(peripheral.call('back', 'getDocs'))
print_r(peripheral.call('back', 'getCraftingCPUs'))

term.redirect(term.native())

screen = peripheral.wrap('left')
screen.clear()

local mainFrame = basalt.createFrame("frame"):setMonitor('left')

mainFrame:addLayout("monitoring.xml")
    :setSize(143,81)

basalt.autoUpdate() 
