require('/spincraft/autoload')

local mainFrame = basalt.createFrame("frame"):setMonitor('left')

basalt.setVariable("menuBarChange", function(self,event,menuBar,x,y)

  local modulesDropdown = mainFrame:getDeepObject("modulesDropdown")

  if modulesDropdown == nil then
    return
  end

  modulesDropdown:show()
end)

mainFrame:addLayout("/spincraft/spincraft.xml")
    :setSize(71,40)

local CPUThread = mainFrame:addThread()

local function CPUThreadFunction()

  local function updateMonitor(frameId, objectId, cpu)

    local frame = mainFrame:getDeepObject(string.lower(frameId))

    if frame == nil then
      return
    end
    
    local info = frame:getDeepObject(string.lower(frameId).."Info")

    if info == nil then
      return
    end
    
    local infoString = ""

    for i = 0, cpu['coprocessors'], 1 do
      infoString = infoString.."@"
    end

    infoString = infoString.."   "..(cpu['storage'] / 1024).."K"

    info:setText(infoString):setPosition("(parent.w * 0.5) - (self.w * 0.5)", 3)
  end


  while true do

    local cpus = peripheral.call('back', 'getCraftingCPUs')

    print_r(cpus)
    
    for key,cpu in pairs(cpus) do
      --print_r(cpu)
      if cpu then
        updateMonitor(string.lower(cpu['name']), nil, cpu)
      end
    end

    os.sleep(1)
  end
end

CPUThread:start(CPUThreadFunction)


basalt.autoUpdate() 
