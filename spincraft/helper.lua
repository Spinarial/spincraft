function writeAt(screen, x, y, text)

    screen.setCursorPos(x, y)
    if(defaultTextColor == nil) then
        defaultTextColor = colors.white
    end

    screen.setTextColor(defaultTextColor)

    if(defaultTextBackground == nil) then
        defaultTextBackground = colors.black
    end

    screen.setBackgroundColor(defaultTextBackground)
    
    screen.write(text)
end

function log(log)
    print("["..textutils.formatTime(os.time(), true).."] "..log)
end

table.hasValue = function(tab, val)
    for index, value in pairs(tab) do
        if value == val then
            return true
        end
    end

    return false
end

table.length = function(arr)

    if(type(arr) ~= "table") then
        error("arg #1 is not a table")
    end

    local indent = 0
    for i, v in pairs(arr) do
        indent = indent + 1
    end

    return indent
end

math.randomseed(os.time())
function randomColor()
    local colors_list = {"white", "orange", "magenta", "lightBlue", "yellow", "lime", "pink", "gray", "lightGray", "cyan", "purple", "blue", "brown", "green", "red", "black"}
    return colors[colors_list[math.random(16)]]
end

