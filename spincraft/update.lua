
require('/spincraft/autoload')
nb_per_page = 20

url = "https://gitlab.com/api/v4/projects/18564042/repository/tree?recursive=true&ref=master"
url = url.."&per_page="..nb_per_page
url = url.."&_cacheid="..os.date('%Y%m%d%H:%M:%S')

screen = peripheral.wrap('right')
screen.clear()
screen.setTextScale(0.5)
screen.setCursorPos(1,1) 

term.redirect(screen)

function createFile(file)

    filePath = '/'..file['path']

    if fs.exists(filePath) == true then
        fs.delete(filePath)
    end

    if file['type'] == 'tree' then

        if fs.isDir(filePath) ~= true then
            fs.makeDir(filePath)
            print('Created folder '..filePath)
        end
    else

        fileRequest = http.request(
            "https://gitlab.com/api/v4/projects/18564042/repository/files/"..urlencode(file['path']).."?ref=master", 
            nil,
            {['PRIVATE-TOKEN'] = 'glpat-UyRUiP1p47pRhCwmdJYt'}
            )

        local fileEvent, fileUrl, fileHandle
        repeat
            fileEvent, fileUrl, fileHandle = os.pullEvent("http_success")
        until fileUrl == fileUrl

        newFileData = textutils.unserializeJSON(fileHandle.readAll())

        newFile = fs.open(filePath, 'w')
        newFile.write(b64dec(newFileData['content']))
        newFile.close()
        print('Created file '..filePath)
    end

end

requestNb = 1

repeat

    newRequest = http.request(url.."&page="..requestNb,nil,{
        ['PRIVATE-TOKEN'] = 'glpat-UyRUiP1p47pRhCwmdJYt',
        ['Cache-Control'] = 'max-age=5'
    })

    local event, url, handle
    repeat
        event, url, handle = os.pullEvent("http_success")
    until url == url

    print("Request n°"..requestNb)
    requestNb = requestNb + 1
    
    files = textutils.unserializeJSON(handle.readAll())
    handle.close()

    for i1,file in pairs(files) do
        createFile(file)
    end

until #files < nb_per_page

term.redirect(term.native())
